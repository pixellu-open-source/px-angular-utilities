import { __decorate, __assign, __spread } from 'tslib';
import { ɵɵdefineInjectable, Injectable, ɵɵinject } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

var PxWindowRef = /** @class */ (function () {
    function PxWindowRef() {
    }
    Object.defineProperty(PxWindowRef.prototype, "nativeElement", {
        get: function () {
            return window;
        },
        enumerable: true,
        configurable: true
    });
    PxWindowRef.ɵprov = ɵɵdefineInjectable({ factory: function PxWindowRef_Factory() { return new PxWindowRef(); }, token: PxWindowRef, providedIn: "root" });
    PxWindowRef = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], PxWindowRef);
    return PxWindowRef;
}());

var DEFAULT_EXPOSE_OPTIONS = {
    sourceWindowArg: false
};
var PxPostalService = /** @class */ (function () {
    function PxPostalService(window) {
        this.window = window;
        this.targetOrigin = '*';
        this.exposedFuncs = new Map();
        this.pendingCallbacks = new Map();
    }
    /**
     * Exposes the function by the given name.
     * @param funcName
     * @param func
     * @param thisArg
     * @param options sourceWindowArg -- if true, the first argument to the exposed function will be the source window.
     */
    PxPostalService.prototype.expose = function (funcName, func, thisArg, options) {
        if (options === void 0) { options = {}; }
        options = __assign(__assign({}, DEFAULT_EXPOSE_OPTIONS), options);
        this.exposedFuncs.set(funcName, { func: func.bind(thisArg), options: options });
        this.updateListener();
    };
    PxPostalService.prototype.unexpose = function (funcName) {
        this.exposedFuncs.delete(funcName);
        this.updateListener();
    };
    PxPostalService.prototype.unexposeAll = function () {
        this.exposedFuncs.clear();
        this.updateListener();
    };
    PxPostalService.prototype.receiveCall = function (message, source) {
        var _a, _b;
        var callFunc = message.callFunc, params = message.params, callbackId = message.callbackId;
        var exposedFunc = this.exposedFuncs.get(callFunc);
        console.assert(!!exposedFunc, 'Call function not found');
        var returnValue;
        // If no listeners
        if (!exposedFunc) {
            return;
        }
        // Check if we should provide source window as the first argument to the exposed function:
        if (exposedFunc.options.sourceWindowArg) {
            returnValue = (_a = this.exposedFuncs.get(callFunc)).func.apply(_a, __spread([source], params));
        }
        else {
            returnValue = (_b = this.exposedFuncs.get(callFunc)).func.apply(_b, __spread(params));
        }
        if (returnValue || returnValue === null) {
            this.sendCallback(callFunc, callbackId, returnValue, source);
        }
    };
    PxPostalService.prototype.updateListener = function () {
        // Remove the subscribe if there are exposed functions:
        if (this.exposedFuncs.size > 0 && (!this.eventSubscription || this.eventSubscription.closed)) {
            this.eventSubscription = fromEvent(this.window.nativeElement, 'message')
                .pipe(map(this.onMessage.bind(this)))
                .subscribe();
            return;
        }
        // Unsubscribe if there are _no_ exposed functions:
        if (this.exposedFuncs.size === 0 && !!this.eventSubscription && !this.eventSubscription.closed) {
            this.eventSubscription.unsubscribe();
        }
    };
    PxPostalService.prototype.receiveCallback = function (message) {
        var callbackId = message.callbackId, returnValue = message.returnValue;
        var subject = this.pendingCallbacks.get(callbackId);
        subject.next(returnValue);
        // TODO: Ensure that subject is destroyed:
        subject.unsubscribe();
        this.pendingCallbacks.delete(callbackId);
    };
    PxPostalService.prototype.sendCall = function (target, funcName) {
        var params = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            params[_i - 2] = arguments[_i];
        }
        var callbackObservable = new Subject();
        var callbackId = this.generateCallbackId();
        this.pendingCallbacks.set(callbackId, callbackObservable);
        target.postMessage({
            callFunc: funcName,
            params: params,
            callbackId: callbackId
        }, this.targetOrigin);
        return callbackObservable;
    };
    PxPostalService.prototype.sendCallback = function (funcName, callbackId, returnValue, target) {
        target.postMessage({
            returnFunc: funcName,
            returnValue: returnValue,
            callbackId: callbackId
        }, this.targetOrigin);
    };
    PxPostalService.prototype.generateCallbackId = function () {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    };
    PxPostalService.prototype.onMessage = function (event) {
        if (event.data.callFunc) {
            this.receiveCall(event.data, event.source);
        }
        else if (event.data.returnFunc) {
            this.receiveCallback(event.data);
        }
    };
    PxPostalService.ctorParameters = function () { return [
        { type: PxWindowRef }
    ]; };
    PxPostalService.ɵprov = ɵɵdefineInjectable({ factory: function PxPostalService_Factory() { return new PxPostalService(ɵɵinject(PxWindowRef)); }, token: PxPostalService, providedIn: "root" });
    PxPostalService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], PxPostalService);
    return PxPostalService;
}());

/*
 * Public API Surface of px-angular-utilities
 */

/**
 * Generated bundle index. Do not edit.
 */

export { PxPostalService, PxWindowRef };
//# sourceMappingURL=px-angular-utilities.js.map
