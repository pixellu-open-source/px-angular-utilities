import { __assign, __decorate, __read, __spread } from "tslib";
import { Injectable } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { PxWindowRef } from '../helpers/px-window-ref';
import * as i0 from "@angular/core";
import * as i1 from "../helpers/px-window-ref";
var DEFAULT_EXPOSE_OPTIONS = {
    sourceWindowArg: false
};
var PxPostalService = /** @class */ (function () {
    function PxPostalService(window) {
        this.window = window;
        this.targetOrigin = '*';
        this.exposedFuncs = new Map();
        this.pendingCallbacks = new Map();
    }
    /**
     * Exposes the function by the given name.
     * @param funcName
     * @param func
     * @param thisArg
     * @param options sourceWindowArg -- if true, the first argument to the exposed function will be the source window.
     */
    PxPostalService.prototype.expose = function (funcName, func, thisArg, options) {
        if (options === void 0) { options = {}; }
        options = __assign(__assign({}, DEFAULT_EXPOSE_OPTIONS), options);
        this.exposedFuncs.set(funcName, { func: func.bind(thisArg), options: options });
        this.updateListener();
    };
    PxPostalService.prototype.unexpose = function (funcName) {
        this.exposedFuncs.delete(funcName);
        this.updateListener();
    };
    PxPostalService.prototype.unexposeAll = function () {
        this.exposedFuncs.clear();
        this.updateListener();
    };
    PxPostalService.prototype.receiveCall = function (message, source) {
        var _a, _b;
        var callFunc = message.callFunc, params = message.params, callbackId = message.callbackId;
        var exposedFunc = this.exposedFuncs.get(callFunc);
        console.assert(!!exposedFunc, 'Call function not found');
        var returnValue;
        // If no listeners
        if (!exposedFunc) {
            return;
        }
        // Check if we should provide source window as the first argument to the exposed function:
        if (exposedFunc.options.sourceWindowArg) {
            returnValue = (_a = this.exposedFuncs.get(callFunc)).func.apply(_a, __spread([source], params));
        }
        else {
            returnValue = (_b = this.exposedFuncs.get(callFunc)).func.apply(_b, __spread(params));
        }
        if (returnValue || returnValue === null) {
            this.sendCallback(callFunc, callbackId, returnValue, source);
        }
    };
    PxPostalService.prototype.updateListener = function () {
        // Remove the subscribe if there are exposed functions:
        if (this.exposedFuncs.size > 0 && (!this.eventSubscription || this.eventSubscription.closed)) {
            this.eventSubscription = fromEvent(this.window.nativeElement, 'message')
                .pipe(map(this.onMessage.bind(this)))
                .subscribe();
            return;
        }
        // Unsubscribe if there are _no_ exposed functions:
        if (this.exposedFuncs.size === 0 && !!this.eventSubscription && !this.eventSubscription.closed) {
            this.eventSubscription.unsubscribe();
        }
    };
    PxPostalService.prototype.receiveCallback = function (message) {
        var callbackId = message.callbackId, returnValue = message.returnValue;
        var subject = this.pendingCallbacks.get(callbackId);
        subject.next(returnValue);
        // TODO: Ensure that subject is destroyed:
        subject.unsubscribe();
        this.pendingCallbacks.delete(callbackId);
    };
    PxPostalService.prototype.sendCall = function (target, funcName) {
        var params = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            params[_i - 2] = arguments[_i];
        }
        var callbackObservable = new Subject();
        var callbackId = this.generateCallbackId();
        this.pendingCallbacks.set(callbackId, callbackObservable);
        target.postMessage({
            callFunc: funcName,
            params: params,
            callbackId: callbackId
        }, this.targetOrigin);
        return callbackObservable;
    };
    PxPostalService.prototype.sendCallback = function (funcName, callbackId, returnValue, target) {
        target.postMessage({
            returnFunc: funcName,
            returnValue: returnValue,
            callbackId: callbackId
        }, this.targetOrigin);
    };
    PxPostalService.prototype.generateCallbackId = function () {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    };
    PxPostalService.prototype.onMessage = function (event) {
        if (event.data.callFunc) {
            this.receiveCall(event.data, event.source);
        }
        else if (event.data.returnFunc) {
            this.receiveCallback(event.data);
        }
    };
    PxPostalService.ctorParameters = function () { return [
        { type: PxWindowRef }
    ]; };
    PxPostalService.ɵprov = i0.ɵɵdefineInjectable({ factory: function PxPostalService_Factory() { return new PxPostalService(i0.ɵɵinject(i1.PxWindowRef)); }, token: PxPostalService, providedIn: "root" });
    PxPostalService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], PxPostalService);
    return PxPostalService;
}());
export { PxPostalService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHgtcG9zdGFsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9weC1hbmd1bGFyLXV0aWxpdGllcy8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9weC1wb3N0YWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsU0FBUyxFQUFjLE9BQU8sRUFBZSxNQUFNLE1BQU0sQ0FBQztBQUNsRSxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkMsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLDBCQUEwQixDQUFDOzs7QUEyQnJELElBQU0sc0JBQXNCLEdBQWtCO0lBQzVDLGVBQWUsRUFBRSxLQUFLO0NBQ3ZCLENBQUM7QUFNRjtJQVFFLHlCQUFvQixNQUFtQjtRQUFuQixXQUFNLEdBQU4sTUFBTSxDQUFhO1FBTnZDLGlCQUFZLEdBQUcsR0FBRyxDQUFDO1FBRVgsaUJBQVksR0FBRyxJQUFJLEdBQUcsRUFBeUIsQ0FBQztRQUNoRCxxQkFBZ0IsR0FBRyxJQUFJLEdBQUcsRUFBNEIsQ0FBQztJQUkvRCxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0gsZ0NBQU0sR0FBTixVQUFPLFFBQWdCLEVBQUUsSUFBYSxFQUFFLE9BQVksRUFBRSxPQUEyQjtRQUEzQix3QkFBQSxFQUFBLFlBQTJCO1FBQy9FLE9BQU8seUJBQU8sc0JBQXNCLEdBQUssT0FBTyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsT0FBTyxTQUFBLEVBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsa0NBQVEsR0FBUixVQUFTLFFBQWdCO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFTyxxQ0FBVyxHQUFuQixVQUFvQixPQUFvQixFQUFFLE1BQVc7O1FBQzVDLElBQUEsMkJBQVEsRUFBRSx1QkFBTSxFQUFFLCtCQUFVLENBQVk7UUFDL0MsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDcEQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLHlCQUF5QixDQUFDLENBQUM7UUFFekQsSUFBSSxXQUFXLENBQUM7UUFFaEIsa0JBQWtCO1FBQ2xCLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDaEIsT0FBTztTQUNSO1FBQ0QsMEZBQTBGO1FBQzFGLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7WUFDdkMsV0FBVyxHQUFHLENBQUEsS0FBQSxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQSxDQUFDLElBQUkscUJBQUMsTUFBTSxHQUFLLE1BQU0sRUFBQyxDQUFDO1NBQ3ZFO2FBQU07WUFDTCxXQUFXLEdBQUcsQ0FBQSxLQUFBLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBLENBQUMsSUFBSSxvQkFBSSxNQUFNLEVBQUMsQ0FBQztTQUMvRDtRQUVELElBQUksV0FBVyxJQUFJLFdBQVcsS0FBSyxJQUFJLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUM5RDtJQUNILENBQUM7SUFFTyx3Q0FBYyxHQUF0QjtRQUNFLHVEQUF1RDtRQUN2RCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUM1RixJQUFJLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLFNBQVMsQ0FBQztpQkFDckUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNwQyxTQUFTLEVBQUUsQ0FBQztZQUNmLE9BQU87U0FDUjtRQUNELG1EQUFtRDtRQUNuRCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRTtZQUM5RixJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDdEM7SUFDSCxDQUFDO0lBRU8seUNBQWUsR0FBdkIsVUFBd0IsT0FBd0I7UUFDdkMsSUFBQSwrQkFBVSxFQUFFLGlDQUFXLENBQVk7UUFDMUMsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0RCxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzFCLDBDQUEwQztRQUMxQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sa0NBQVEsR0FBZixVQUFnQixNQUFNLEVBQUUsUUFBUTtRQUFFLGdCQUFnQjthQUFoQixVQUFnQixFQUFoQixxQkFBZ0IsRUFBaEIsSUFBZ0I7WUFBaEIsK0JBQWdCOztRQUNoRCxJQUFNLGtCQUFrQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDekMsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUUxRCxNQUFNLENBQUMsV0FBVyxDQUFDO1lBQ2pCLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLE1BQU0sUUFBQTtZQUNOLFVBQVUsWUFBQTtTQUNJLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXJDLE9BQU8sa0JBQWtCLENBQUM7SUFDNUIsQ0FBQztJQUVPLHNDQUFZLEdBQXBCLFVBQXFCLFFBQWtCLEVBQUUsVUFBc0IsRUFBRSxXQUFXLEVBQUUsTUFBTTtRQUNsRixNQUFNLENBQUMsV0FBVyxDQUFDO1lBQ2pCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLFdBQVcsYUFBQTtZQUNYLFVBQVUsWUFBQTtTQUNRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTyw0Q0FBa0IsR0FBMUI7UUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDbkcsQ0FBQztJQUVPLG1DQUFTLEdBQWpCLFVBQWtCLEtBQW1CO1FBQ25DLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUM1QzthQUFNLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDaEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEM7SUFDSCxDQUFDOztnQkF4RzJCLFdBQVc7OztJQVI1QixlQUFlO1FBSDNCLFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7T0FDVyxlQUFlLENBaUgzQjswQkF2SkQ7Q0F1SkMsQUFqSEQsSUFpSEM7U0FqSFksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge2Zyb21FdmVudCwgT2JzZXJ2YWJsZSwgU3ViamVjdCwgU3Vic2NyaXB0aW9ufSBmcm9tICdyeGpzJztcbmltcG9ydCB7bWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQge1B4V2luZG93UmVmfSBmcm9tICcuLi9oZWxwZXJzL3B4LXdpbmRvdy1yZWYnO1xuXG50eXBlIENhbGxiYWNrSWQgPSBzdHJpbmc7XG50eXBlIEZ1bmNOYW1lID0gc3RyaW5nO1xudHlwZSBBbnlGdW5jID0gKC4uLmFyZ3M6IGFueVtdKSA9PiBhbnk7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRXhwb3NlT3B0aW9ucyB7XG4gIHNvdXJjZVdpbmRvd0FyZz86IGJvb2xlYW47XG59XG5cbmludGVyZmFjZSBFeHBvc2VkRnVuYyB7XG4gIGZ1bmM6IEFueUZ1bmMsXG4gIG9wdGlvbnM6IEV4cG9zZU9wdGlvbnNcbn1cblxuaW50ZXJmYWNlIENhbGxNZXNzYWdlIHtcbiAgY2FsbEZ1bmM6IEZ1bmNOYW1lO1xuICBwYXJhbXM6IGFueVtdO1xuICBjYWxsYmFja0lkOiBDYWxsYmFja0lkO1xufVxuXG5pbnRlcmZhY2UgQ2FsbGJhY2tNZXNzYWdlIHtcbiAgcmV0dXJuRnVuYzogRnVuY05hbWU7XG4gIHJldHVyblZhbHVlOiBhbnk7XG4gIGNhbGxiYWNrSWQ6IENhbGxiYWNrSWQ7XG59XG5cbmNvbnN0IERFRkFVTFRfRVhQT1NFX09QVElPTlM6IEV4cG9zZU9wdGlvbnMgPSB7XG4gIHNvdXJjZVdpbmRvd0FyZzogZmFsc2Vcbn07XG5cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUHhQb3N0YWxTZXJ2aWNlIHtcblxuICB0YXJnZXRPcmlnaW4gPSAnKic7XG5cbiAgcHJpdmF0ZSBleHBvc2VkRnVuY3MgPSBuZXcgTWFwPEZ1bmNOYW1lLCBFeHBvc2VkRnVuYz4oKTtcbiAgcHJpdmF0ZSBwZW5kaW5nQ2FsbGJhY2tzID0gbmV3IE1hcDxDYWxsYmFja0lkLCBTdWJqZWN0PGFueT4+KCk7XG4gIHByaXZhdGUgZXZlbnRTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHdpbmRvdzogUHhXaW5kb3dSZWYpIHtcbiAgfVxuXG4gIC8qKlxuICAgKiBFeHBvc2VzIHRoZSBmdW5jdGlvbiBieSB0aGUgZ2l2ZW4gbmFtZS5cbiAgICogQHBhcmFtIGZ1bmNOYW1lXG4gICAqIEBwYXJhbSBmdW5jXG4gICAqIEBwYXJhbSB0aGlzQXJnXG4gICAqIEBwYXJhbSBvcHRpb25zIHNvdXJjZVdpbmRvd0FyZyAtLSBpZiB0cnVlLCB0aGUgZmlyc3QgYXJndW1lbnQgdG8gdGhlIGV4cG9zZWQgZnVuY3Rpb24gd2lsbCBiZSB0aGUgc291cmNlIHdpbmRvdy5cbiAgICovXG4gIGV4cG9zZShmdW5jTmFtZTogc3RyaW5nLCBmdW5jOiBBbnlGdW5jLCB0aGlzQXJnOiBhbnksIG9wdGlvbnM6IEV4cG9zZU9wdGlvbnMgPSB7fSkge1xuICAgIG9wdGlvbnMgPSB7Li4uREVGQVVMVF9FWFBPU0VfT1BUSU9OUywgLi4ub3B0aW9uc307XG4gICAgdGhpcy5leHBvc2VkRnVuY3Muc2V0KGZ1bmNOYW1lLCB7ZnVuYzogZnVuYy5iaW5kKHRoaXNBcmcpLCBvcHRpb25zfSk7XG4gICAgdGhpcy51cGRhdGVMaXN0ZW5lcigpO1xuICB9XG5cbiAgdW5leHBvc2UoZnVuY05hbWU6IHN0cmluZykge1xuICAgIHRoaXMuZXhwb3NlZEZ1bmNzLmRlbGV0ZShmdW5jTmFtZSk7XG4gICAgdGhpcy51cGRhdGVMaXN0ZW5lcigpO1xuICB9XG5cbiAgdW5leHBvc2VBbGwoKSB7XG4gICAgdGhpcy5leHBvc2VkRnVuY3MuY2xlYXIoKTtcbiAgICB0aGlzLnVwZGF0ZUxpc3RlbmVyKCk7XG4gIH1cblxuICBwcml2YXRlIHJlY2VpdmVDYWxsKG1lc3NhZ2U6IENhbGxNZXNzYWdlLCBzb3VyY2U6IGFueSkge1xuICAgIGNvbnN0IHtjYWxsRnVuYywgcGFyYW1zLCBjYWxsYmFja0lkfSA9IG1lc3NhZ2U7XG4gICAgY29uc3QgZXhwb3NlZEZ1bmMgPSB0aGlzLmV4cG9zZWRGdW5jcy5nZXQoY2FsbEZ1bmMpO1xuICAgIGNvbnNvbGUuYXNzZXJ0KCEhZXhwb3NlZEZ1bmMsICdDYWxsIGZ1bmN0aW9uIG5vdCBmb3VuZCcpO1xuXG4gICAgbGV0IHJldHVyblZhbHVlO1xuXG4gICAgLy8gSWYgbm8gbGlzdGVuZXJzXG4gICAgaWYgKCFleHBvc2VkRnVuYykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICAvLyBDaGVjayBpZiB3ZSBzaG91bGQgcHJvdmlkZSBzb3VyY2Ugd2luZG93IGFzIHRoZSBmaXJzdCBhcmd1bWVudCB0byB0aGUgZXhwb3NlZCBmdW5jdGlvbjpcbiAgICBpZiAoZXhwb3NlZEZ1bmMub3B0aW9ucy5zb3VyY2VXaW5kb3dBcmcpIHtcbiAgICAgIHJldHVyblZhbHVlID0gdGhpcy5leHBvc2VkRnVuY3MuZ2V0KGNhbGxGdW5jKS5mdW5jKHNvdXJjZSwgLi4ucGFyYW1zKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuVmFsdWUgPSB0aGlzLmV4cG9zZWRGdW5jcy5nZXQoY2FsbEZ1bmMpLmZ1bmMoLi4ucGFyYW1zKTtcbiAgICB9XG5cbiAgICBpZiAocmV0dXJuVmFsdWUgfHwgcmV0dXJuVmFsdWUgPT09IG51bGwpIHtcbiAgICAgIHRoaXMuc2VuZENhbGxiYWNrKGNhbGxGdW5jLCBjYWxsYmFja0lkLCByZXR1cm5WYWx1ZSwgc291cmNlKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHVwZGF0ZUxpc3RlbmVyKCkge1xuICAgIC8vIFJlbW92ZSB0aGUgc3Vic2NyaWJlIGlmIHRoZXJlIGFyZSBleHBvc2VkIGZ1bmN0aW9uczpcbiAgICBpZiAodGhpcy5leHBvc2VkRnVuY3Muc2l6ZSA+IDAgJiYgKCF0aGlzLmV2ZW50U3Vic2NyaXB0aW9uIHx8IHRoaXMuZXZlbnRTdWJzY3JpcHRpb24uY2xvc2VkKSkge1xuICAgICAgdGhpcy5ldmVudFN1YnNjcmlwdGlvbiA9IGZyb21FdmVudCh0aGlzLndpbmRvdy5uYXRpdmVFbGVtZW50LCAnbWVzc2FnZScpXG4gICAgICAgIC5waXBlKG1hcCh0aGlzLm9uTWVzc2FnZS5iaW5kKHRoaXMpKSlcbiAgICAgICAgLnN1YnNjcmliZSgpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICAvLyBVbnN1YnNjcmliZSBpZiB0aGVyZSBhcmUgX25vXyBleHBvc2VkIGZ1bmN0aW9uczpcbiAgICBpZiAodGhpcy5leHBvc2VkRnVuY3Muc2l6ZSA9PT0gMCAmJiAhIXRoaXMuZXZlbnRTdWJzY3JpcHRpb24gJiYgIXRoaXMuZXZlbnRTdWJzY3JpcHRpb24uY2xvc2VkKSB7XG4gICAgICB0aGlzLmV2ZW50U3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSByZWNlaXZlQ2FsbGJhY2sobWVzc2FnZTogQ2FsbGJhY2tNZXNzYWdlKSB7XG4gICAgY29uc3Qge2NhbGxiYWNrSWQsIHJldHVyblZhbHVlfSA9IG1lc3NhZ2U7XG4gICAgY29uc3Qgc3ViamVjdCA9IHRoaXMucGVuZGluZ0NhbGxiYWNrcy5nZXQoY2FsbGJhY2tJZCk7XG4gICAgc3ViamVjdC5uZXh0KHJldHVyblZhbHVlKTtcbiAgICAvLyBUT0RPOiBFbnN1cmUgdGhhdCBzdWJqZWN0IGlzIGRlc3Ryb3llZDpcbiAgICBzdWJqZWN0LnVuc3Vic2NyaWJlKCk7XG4gICAgdGhpcy5wZW5kaW5nQ2FsbGJhY2tzLmRlbGV0ZShjYWxsYmFja0lkKTtcbiAgfVxuXG4gIHB1YmxpYyBzZW5kQ2FsbCh0YXJnZXQsIGZ1bmNOYW1lLCAuLi5wYXJhbXM6IGFueVtdKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zdCBjYWxsYmFja09ic2VydmFibGUgPSBuZXcgU3ViamVjdCgpO1xuICAgIGNvbnN0IGNhbGxiYWNrSWQgPSB0aGlzLmdlbmVyYXRlQ2FsbGJhY2tJZCgpO1xuICAgIHRoaXMucGVuZGluZ0NhbGxiYWNrcy5zZXQoY2FsbGJhY2tJZCwgY2FsbGJhY2tPYnNlcnZhYmxlKTtcblxuICAgIHRhcmdldC5wb3N0TWVzc2FnZSh7XG4gICAgICBjYWxsRnVuYzogZnVuY05hbWUsXG4gICAgICBwYXJhbXMsXG4gICAgICBjYWxsYmFja0lkXG4gICAgfSBhcyBDYWxsTWVzc2FnZSwgdGhpcy50YXJnZXRPcmlnaW4pO1xuXG4gICAgcmV0dXJuIGNhbGxiYWNrT2JzZXJ2YWJsZTtcbiAgfVxuXG4gIHByaXZhdGUgc2VuZENhbGxiYWNrKGZ1bmNOYW1lOiBGdW5jTmFtZSwgY2FsbGJhY2tJZDogQ2FsbGJhY2tJZCwgcmV0dXJuVmFsdWUsIHRhcmdldCkge1xuICAgIHRhcmdldC5wb3N0TWVzc2FnZSh7XG4gICAgICByZXR1cm5GdW5jOiBmdW5jTmFtZSxcbiAgICAgIHJldHVyblZhbHVlLFxuICAgICAgY2FsbGJhY2tJZFxuICAgIH0gYXMgQ2FsbGJhY2tNZXNzYWdlLCB0aGlzLnRhcmdldE9yaWdpbik7XG4gIH1cblxuICBwcml2YXRlIGdlbmVyYXRlQ2FsbGJhY2tJZCgpOiBDYWxsYmFja0lkIHtcbiAgICByZXR1cm4gTWF0aC5yYW5kb20oKS50b1N0cmluZygzNikuc3Vic3RyaW5nKDIsIDE1KSArIE1hdGgucmFuZG9tKCkudG9TdHJpbmcoMzYpLnN1YnN0cmluZygyLCAxNSk7XG4gIH1cblxuICBwcml2YXRlIG9uTWVzc2FnZShldmVudDogTWVzc2FnZUV2ZW50KSB7XG4gICAgaWYgKGV2ZW50LmRhdGEuY2FsbEZ1bmMpIHtcbiAgICAgIHRoaXMucmVjZWl2ZUNhbGwoZXZlbnQuZGF0YSwgZXZlbnQuc291cmNlKTtcbiAgICB9IGVsc2UgaWYgKGV2ZW50LmRhdGEucmV0dXJuRnVuYykge1xuICAgICAgdGhpcy5yZWNlaXZlQ2FsbGJhY2soZXZlbnQuZGF0YSk7XG4gICAgfVxuICB9XG59XG5cbiJdfQ==