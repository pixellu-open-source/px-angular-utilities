import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var PxWindowRef = /** @class */ (function () {
    function PxWindowRef() {
    }
    Object.defineProperty(PxWindowRef.prototype, "nativeElement", {
        get: function () {
            return window;
        },
        enumerable: true,
        configurable: true
    });
    PxWindowRef.ɵprov = i0.ɵɵdefineInjectable({ factory: function PxWindowRef_Factory() { return new PxWindowRef(); }, token: PxWindowRef, providedIn: "root" });
    PxWindowRef = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], PxWindowRef);
    return PxWindowRef;
}());
export { PxWindowRef };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHgtd2luZG93LXJlZi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3B4LWFuZ3VsYXItdXRpbGl0aWVzLyIsInNvdXJjZXMiOlsibGliL2hlbHBlcnMvcHgtd2luZG93LXJlZi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQzs7QUFLekM7SUFBQTtLQUlDO0lBSEMsc0JBQUksc0NBQWE7YUFBakI7WUFDRSxPQUFPLE1BQU0sQ0FBQztRQUNoQixDQUFDOzs7T0FBQTs7SUFIVSxXQUFXO1FBSHZCLFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7T0FDVyxXQUFXLENBSXZCO3NCQVREO0NBU0MsQUFKRCxJQUlDO1NBSlksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFB4V2luZG93UmVmIHtcbiAgZ2V0IG5hdGl2ZUVsZW1lbnQoKTogV2luZG93IHtcbiAgICByZXR1cm4gd2luZG93O1xuICB9XG59XG4iXX0=