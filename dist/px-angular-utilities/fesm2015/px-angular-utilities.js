import { __decorate } from 'tslib';
import { ɵɵdefineInjectable, Injectable, ɵɵinject } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

let PxWindowRef = class PxWindowRef {
    get nativeElement() {
        return window;
    }
};
PxWindowRef.ɵprov = ɵɵdefineInjectable({ factory: function PxWindowRef_Factory() { return new PxWindowRef(); }, token: PxWindowRef, providedIn: "root" });
PxWindowRef = __decorate([
    Injectable({
        providedIn: 'root'
    })
], PxWindowRef);

const DEFAULT_EXPOSE_OPTIONS = {
    sourceWindowArg: false
};
let PxPostalService = class PxPostalService {
    constructor(window) {
        this.window = window;
        this.targetOrigin = '*';
        this.exposedFuncs = new Map();
        this.pendingCallbacks = new Map();
    }
    /**
     * Exposes the function by the given name.
     * @param funcName
     * @param func
     * @param thisArg
     * @param options sourceWindowArg -- if true, the first argument to the exposed function will be the source window.
     */
    expose(funcName, func, thisArg, options = {}) {
        options = Object.assign(Object.assign({}, DEFAULT_EXPOSE_OPTIONS), options);
        this.exposedFuncs.set(funcName, { func: func.bind(thisArg), options });
        this.updateListener();
    }
    unexpose(funcName) {
        this.exposedFuncs.delete(funcName);
        this.updateListener();
    }
    unexposeAll() {
        this.exposedFuncs.clear();
        this.updateListener();
    }
    receiveCall(message, source) {
        const { callFunc, params, callbackId } = message;
        const exposedFunc = this.exposedFuncs.get(callFunc);
        console.assert(!!exposedFunc, 'Call function not found');
        let returnValue;
        // If no listeners
        if (!exposedFunc) {
            return;
        }
        // Check if we should provide source window as the first argument to the exposed function:
        if (exposedFunc.options.sourceWindowArg) {
            returnValue = this.exposedFuncs.get(callFunc).func(source, ...params);
        }
        else {
            returnValue = this.exposedFuncs.get(callFunc).func(...params);
        }
        if (returnValue || returnValue === null) {
            this.sendCallback(callFunc, callbackId, returnValue, source);
        }
    }
    updateListener() {
        // Remove the subscribe if there are exposed functions:
        if (this.exposedFuncs.size > 0 && (!this.eventSubscription || this.eventSubscription.closed)) {
            this.eventSubscription = fromEvent(this.window.nativeElement, 'message')
                .pipe(map(this.onMessage.bind(this)))
                .subscribe();
            return;
        }
        // Unsubscribe if there are _no_ exposed functions:
        if (this.exposedFuncs.size === 0 && !!this.eventSubscription && !this.eventSubscription.closed) {
            this.eventSubscription.unsubscribe();
        }
    }
    receiveCallback(message) {
        const { callbackId, returnValue } = message;
        const subject = this.pendingCallbacks.get(callbackId);
        subject.next(returnValue);
        // TODO: Ensure that subject is destroyed:
        subject.unsubscribe();
        this.pendingCallbacks.delete(callbackId);
    }
    sendCall(target, funcName, ...params) {
        const callbackObservable = new Subject();
        const callbackId = this.generateCallbackId();
        this.pendingCallbacks.set(callbackId, callbackObservable);
        target.postMessage({
            callFunc: funcName,
            params,
            callbackId
        }, this.targetOrigin);
        return callbackObservable;
    }
    sendCallback(funcName, callbackId, returnValue, target) {
        target.postMessage({
            returnFunc: funcName,
            returnValue,
            callbackId
        }, this.targetOrigin);
    }
    generateCallbackId() {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }
    onMessage(event) {
        if (event.data.callFunc) {
            this.receiveCall(event.data, event.source);
        }
        else if (event.data.returnFunc) {
            this.receiveCallback(event.data);
        }
    }
};
PxPostalService.ctorParameters = () => [
    { type: PxWindowRef }
];
PxPostalService.ɵprov = ɵɵdefineInjectable({ factory: function PxPostalService_Factory() { return new PxPostalService(ɵɵinject(PxWindowRef)); }, token: PxPostalService, providedIn: "root" });
PxPostalService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], PxPostalService);

/*
 * Public API Surface of px-angular-utilities
 */

/**
 * Generated bundle index. Do not edit.
 */

export { PxPostalService, PxWindowRef };
//# sourceMappingURL=px-angular-utilities.js.map
