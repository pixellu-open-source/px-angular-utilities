import { Observable } from 'rxjs';
import { PxWindowRef } from '../helpers/px-window-ref';
declare type AnyFunc = (...args: any[]) => any;
export interface ExposeOptions {
    sourceWindowArg?: boolean;
}
export declare class PxPostalService {
    private window;
    targetOrigin: string;
    private exposedFuncs;
    private pendingCallbacks;
    private eventSubscription;
    constructor(window: PxWindowRef);
    /**
     * Exposes the function by the given name.
     * @param funcName
     * @param func
     * @param thisArg
     * @param options sourceWindowArg -- if true, the first argument to the exposed function will be the source window.
     */
    expose(funcName: string, func: AnyFunc, thisArg: any, options?: ExposeOptions): void;
    unexpose(funcName: string): void;
    unexposeAll(): void;
    private receiveCall;
    private updateListener;
    private receiveCallback;
    sendCall(target: any, funcName: any, ...params: any[]): Observable<any>;
    private sendCallback;
    private generateCallbackId;
    private onMessage;
}
export {};
