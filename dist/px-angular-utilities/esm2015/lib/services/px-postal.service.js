import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { PxWindowRef } from '../helpers/px-window-ref';
import * as i0 from "@angular/core";
import * as i1 from "../helpers/px-window-ref";
const DEFAULT_EXPOSE_OPTIONS = {
    sourceWindowArg: false
};
let PxPostalService = class PxPostalService {
    constructor(window) {
        this.window = window;
        this.targetOrigin = '*';
        this.exposedFuncs = new Map();
        this.pendingCallbacks = new Map();
    }
    /**
     * Exposes the function by the given name.
     * @param funcName
     * @param func
     * @param thisArg
     * @param options sourceWindowArg -- if true, the first argument to the exposed function will be the source window.
     */
    expose(funcName, func, thisArg, options = {}) {
        options = Object.assign(Object.assign({}, DEFAULT_EXPOSE_OPTIONS), options);
        this.exposedFuncs.set(funcName, { func: func.bind(thisArg), options });
        this.updateListener();
    }
    unexpose(funcName) {
        this.exposedFuncs.delete(funcName);
        this.updateListener();
    }
    unexposeAll() {
        this.exposedFuncs.clear();
        this.updateListener();
    }
    receiveCall(message, source) {
        const { callFunc, params, callbackId } = message;
        const exposedFunc = this.exposedFuncs.get(callFunc);
        console.assert(!!exposedFunc, 'Call function not found');
        let returnValue;
        // If no listeners
        if (!exposedFunc) {
            return;
        }
        // Check if we should provide source window as the first argument to the exposed function:
        if (exposedFunc.options.sourceWindowArg) {
            returnValue = this.exposedFuncs.get(callFunc).func(source, ...params);
        }
        else {
            returnValue = this.exposedFuncs.get(callFunc).func(...params);
        }
        if (returnValue || returnValue === null) {
            this.sendCallback(callFunc, callbackId, returnValue, source);
        }
    }
    updateListener() {
        // Remove the subscribe if there are exposed functions:
        if (this.exposedFuncs.size > 0 && (!this.eventSubscription || this.eventSubscription.closed)) {
            this.eventSubscription = fromEvent(this.window.nativeElement, 'message')
                .pipe(map(this.onMessage.bind(this)))
                .subscribe();
            return;
        }
        // Unsubscribe if there are _no_ exposed functions:
        if (this.exposedFuncs.size === 0 && !!this.eventSubscription && !this.eventSubscription.closed) {
            this.eventSubscription.unsubscribe();
        }
    }
    receiveCallback(message) {
        const { callbackId, returnValue } = message;
        const subject = this.pendingCallbacks.get(callbackId);
        subject.next(returnValue);
        // TODO: Ensure that subject is destroyed:
        subject.unsubscribe();
        this.pendingCallbacks.delete(callbackId);
    }
    sendCall(target, funcName, ...params) {
        const callbackObservable = new Subject();
        const callbackId = this.generateCallbackId();
        this.pendingCallbacks.set(callbackId, callbackObservable);
        target.postMessage({
            callFunc: funcName,
            params,
            callbackId
        }, this.targetOrigin);
        return callbackObservable;
    }
    sendCallback(funcName, callbackId, returnValue, target) {
        target.postMessage({
            returnFunc: funcName,
            returnValue,
            callbackId
        }, this.targetOrigin);
    }
    generateCallbackId() {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }
    onMessage(event) {
        if (event.data.callFunc) {
            this.receiveCall(event.data, event.source);
        }
        else if (event.data.returnFunc) {
            this.receiveCallback(event.data);
        }
    }
};
PxPostalService.ctorParameters = () => [
    { type: PxWindowRef }
];
PxPostalService.ɵprov = i0.ɵɵdefineInjectable({ factory: function PxPostalService_Factory() { return new PxPostalService(i0.ɵɵinject(i1.PxWindowRef)); }, token: PxPostalService, providedIn: "root" });
PxPostalService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], PxPostalService);
export { PxPostalService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHgtcG9zdGFsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9weC1hbmd1bGFyLXV0aWxpdGllcy8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9weC1wb3N0YWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsU0FBUyxFQUFjLE9BQU8sRUFBZSxNQUFNLE1BQU0sQ0FBQztBQUNsRSxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkMsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLDBCQUEwQixDQUFDOzs7QUEyQnJELE1BQU0sc0JBQXNCLEdBQWtCO0lBQzVDLGVBQWUsRUFBRSxLQUFLO0NBQ3ZCLENBQUM7QUFNRixJQUFhLGVBQWUsR0FBNUIsTUFBYSxlQUFlO0lBUTFCLFlBQW9CLE1BQW1CO1FBQW5CLFdBQU0sR0FBTixNQUFNLENBQWE7UUFOdkMsaUJBQVksR0FBRyxHQUFHLENBQUM7UUFFWCxpQkFBWSxHQUFHLElBQUksR0FBRyxFQUF5QixDQUFDO1FBQ2hELHFCQUFnQixHQUFHLElBQUksR0FBRyxFQUE0QixDQUFDO0lBSS9ELENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSCxNQUFNLENBQUMsUUFBZ0IsRUFBRSxJQUFhLEVBQUUsT0FBWSxFQUFFLFVBQXlCLEVBQUU7UUFDL0UsT0FBTyxtQ0FBTyxzQkFBc0IsR0FBSyxPQUFPLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsRUFBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsUUFBUSxDQUFDLFFBQWdCO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFTyxXQUFXLENBQUMsT0FBb0IsRUFBRSxNQUFXO1FBQ25ELE1BQU0sRUFBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBQyxHQUFHLE9BQU8sQ0FBQztRQUMvQyxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwRCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUseUJBQXlCLENBQUMsQ0FBQztRQUV6RCxJQUFJLFdBQVcsQ0FBQztRQUVoQixrQkFBa0I7UUFDbEIsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNoQixPQUFPO1NBQ1I7UUFDRCwwRkFBMEY7UUFDMUYsSUFBSSxXQUFXLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRTtZQUN2QyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLE1BQU0sQ0FBQyxDQUFDO1NBQ3ZFO2FBQU07WUFDTCxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUM7U0FDL0Q7UUFFRCxJQUFJLFdBQVcsSUFBSSxXQUFXLEtBQUssSUFBSSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDOUQ7SUFDSCxDQUFDO0lBRU8sY0FBYztRQUNwQix1REFBdUQ7UUFDdkQsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDNUYsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUM7aUJBQ3JFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDcEMsU0FBUyxFQUFFLENBQUM7WUFDZixPQUFPO1NBQ1I7UUFDRCxtREFBbUQ7UUFDbkQsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7WUFDOUYsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQztJQUVPLGVBQWUsQ0FBQyxPQUF3QjtRQUM5QyxNQUFNLEVBQUMsVUFBVSxFQUFFLFdBQVcsRUFBQyxHQUFHLE9BQU8sQ0FBQztRQUMxQyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RELE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDMUIsMENBQTBDO1FBQzFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSxRQUFRLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxHQUFHLE1BQWE7UUFDaEQsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3pDLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzdDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFFMUQsTUFBTSxDQUFDLFdBQVcsQ0FBQztZQUNqQixRQUFRLEVBQUUsUUFBUTtZQUNsQixNQUFNO1lBQ04sVUFBVTtTQUNJLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXJDLE9BQU8sa0JBQWtCLENBQUM7SUFDNUIsQ0FBQztJQUVPLFlBQVksQ0FBQyxRQUFrQixFQUFFLFVBQXNCLEVBQUUsV0FBVyxFQUFFLE1BQU07UUFDbEYsTUFBTSxDQUFDLFdBQVcsQ0FBQztZQUNqQixVQUFVLEVBQUUsUUFBUTtZQUNwQixXQUFXO1lBQ1gsVUFBVTtTQUNRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTyxrQkFBa0I7UUFDeEIsT0FBTyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ25HLENBQUM7SUFFTyxTQUFTLENBQUMsS0FBbUI7UUFDbkMsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzVDO2FBQU0sSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsQztJQUNILENBQUM7Q0FDRixDQUFBOztZQXpHNkIsV0FBVzs7O0FBUjVCLGVBQWU7SUFIM0IsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUNXLGVBQWUsQ0FpSDNCO1NBakhZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtmcm9tRXZlbnQsIE9ic2VydmFibGUsIFN1YmplY3QsIFN1YnNjcmlwdGlvbn0gZnJvbSAncnhqcyc7XG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHtQeFdpbmRvd1JlZn0gZnJvbSAnLi4vaGVscGVycy9weC13aW5kb3ctcmVmJztcblxudHlwZSBDYWxsYmFja0lkID0gc3RyaW5nO1xudHlwZSBGdW5jTmFtZSA9IHN0cmluZztcbnR5cGUgQW55RnVuYyA9ICguLi5hcmdzOiBhbnlbXSkgPT4gYW55O1xuXG5leHBvcnQgaW50ZXJmYWNlIEV4cG9zZU9wdGlvbnMge1xuICBzb3VyY2VXaW5kb3dBcmc/OiBib29sZWFuO1xufVxuXG5pbnRlcmZhY2UgRXhwb3NlZEZ1bmMge1xuICBmdW5jOiBBbnlGdW5jLFxuICBvcHRpb25zOiBFeHBvc2VPcHRpb25zXG59XG5cbmludGVyZmFjZSBDYWxsTWVzc2FnZSB7XG4gIGNhbGxGdW5jOiBGdW5jTmFtZTtcbiAgcGFyYW1zOiBhbnlbXTtcbiAgY2FsbGJhY2tJZDogQ2FsbGJhY2tJZDtcbn1cblxuaW50ZXJmYWNlIENhbGxiYWNrTWVzc2FnZSB7XG4gIHJldHVybkZ1bmM6IEZ1bmNOYW1lO1xuICByZXR1cm5WYWx1ZTogYW55O1xuICBjYWxsYmFja0lkOiBDYWxsYmFja0lkO1xufVxuXG5jb25zdCBERUZBVUxUX0VYUE9TRV9PUFRJT05TOiBFeHBvc2VPcHRpb25zID0ge1xuICBzb3VyY2VXaW5kb3dBcmc6IGZhbHNlXG59O1xuXG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFB4UG9zdGFsU2VydmljZSB7XG5cbiAgdGFyZ2V0T3JpZ2luID0gJyonO1xuXG4gIHByaXZhdGUgZXhwb3NlZEZ1bmNzID0gbmV3IE1hcDxGdW5jTmFtZSwgRXhwb3NlZEZ1bmM+KCk7XG4gIHByaXZhdGUgcGVuZGluZ0NhbGxiYWNrcyA9IG5ldyBNYXA8Q2FsbGJhY2tJZCwgU3ViamVjdDxhbnk+PigpO1xuICBwcml2YXRlIGV2ZW50U3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSB3aW5kb3c6IFB4V2luZG93UmVmKSB7XG4gIH1cblxuICAvKipcbiAgICogRXhwb3NlcyB0aGUgZnVuY3Rpb24gYnkgdGhlIGdpdmVuIG5hbWUuXG4gICAqIEBwYXJhbSBmdW5jTmFtZVxuICAgKiBAcGFyYW0gZnVuY1xuICAgKiBAcGFyYW0gdGhpc0FyZ1xuICAgKiBAcGFyYW0gb3B0aW9ucyBzb3VyY2VXaW5kb3dBcmcgLS0gaWYgdHJ1ZSwgdGhlIGZpcnN0IGFyZ3VtZW50IHRvIHRoZSBleHBvc2VkIGZ1bmN0aW9uIHdpbGwgYmUgdGhlIHNvdXJjZSB3aW5kb3cuXG4gICAqL1xuICBleHBvc2UoZnVuY05hbWU6IHN0cmluZywgZnVuYzogQW55RnVuYywgdGhpc0FyZzogYW55LCBvcHRpb25zOiBFeHBvc2VPcHRpb25zID0ge30pIHtcbiAgICBvcHRpb25zID0gey4uLkRFRkFVTFRfRVhQT1NFX09QVElPTlMsIC4uLm9wdGlvbnN9O1xuICAgIHRoaXMuZXhwb3NlZEZ1bmNzLnNldChmdW5jTmFtZSwge2Z1bmM6IGZ1bmMuYmluZCh0aGlzQXJnKSwgb3B0aW9uc30pO1xuICAgIHRoaXMudXBkYXRlTGlzdGVuZXIoKTtcbiAgfVxuXG4gIHVuZXhwb3NlKGZ1bmNOYW1lOiBzdHJpbmcpIHtcbiAgICB0aGlzLmV4cG9zZWRGdW5jcy5kZWxldGUoZnVuY05hbWUpO1xuICAgIHRoaXMudXBkYXRlTGlzdGVuZXIoKTtcbiAgfVxuXG4gIHVuZXhwb3NlQWxsKCkge1xuICAgIHRoaXMuZXhwb3NlZEZ1bmNzLmNsZWFyKCk7XG4gICAgdGhpcy51cGRhdGVMaXN0ZW5lcigpO1xuICB9XG5cbiAgcHJpdmF0ZSByZWNlaXZlQ2FsbChtZXNzYWdlOiBDYWxsTWVzc2FnZSwgc291cmNlOiBhbnkpIHtcbiAgICBjb25zdCB7Y2FsbEZ1bmMsIHBhcmFtcywgY2FsbGJhY2tJZH0gPSBtZXNzYWdlO1xuICAgIGNvbnN0IGV4cG9zZWRGdW5jID0gdGhpcy5leHBvc2VkRnVuY3MuZ2V0KGNhbGxGdW5jKTtcbiAgICBjb25zb2xlLmFzc2VydCghIWV4cG9zZWRGdW5jLCAnQ2FsbCBmdW5jdGlvbiBub3QgZm91bmQnKTtcblxuICAgIGxldCByZXR1cm5WYWx1ZTtcblxuICAgIC8vIElmIG5vIGxpc3RlbmVyc1xuICAgIGlmICghZXhwb3NlZEZ1bmMpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgLy8gQ2hlY2sgaWYgd2Ugc2hvdWxkIHByb3ZpZGUgc291cmNlIHdpbmRvdyBhcyB0aGUgZmlyc3QgYXJndW1lbnQgdG8gdGhlIGV4cG9zZWQgZnVuY3Rpb246XG4gICAgaWYgKGV4cG9zZWRGdW5jLm9wdGlvbnMuc291cmNlV2luZG93QXJnKSB7XG4gICAgICByZXR1cm5WYWx1ZSA9IHRoaXMuZXhwb3NlZEZ1bmNzLmdldChjYWxsRnVuYykuZnVuYyhzb3VyY2UsIC4uLnBhcmFtcyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVyblZhbHVlID0gdGhpcy5leHBvc2VkRnVuY3MuZ2V0KGNhbGxGdW5jKS5mdW5jKC4uLnBhcmFtcyk7XG4gICAgfVxuXG4gICAgaWYgKHJldHVyblZhbHVlIHx8IHJldHVyblZhbHVlID09PSBudWxsKSB7XG4gICAgICB0aGlzLnNlbmRDYWxsYmFjayhjYWxsRnVuYywgY2FsbGJhY2tJZCwgcmV0dXJuVmFsdWUsIHNvdXJjZSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSB1cGRhdGVMaXN0ZW5lcigpIHtcbiAgICAvLyBSZW1vdmUgdGhlIHN1YnNjcmliZSBpZiB0aGVyZSBhcmUgZXhwb3NlZCBmdW5jdGlvbnM6XG4gICAgaWYgKHRoaXMuZXhwb3NlZEZ1bmNzLnNpemUgPiAwICYmICghdGhpcy5ldmVudFN1YnNjcmlwdGlvbiB8fCB0aGlzLmV2ZW50U3Vic2NyaXB0aW9uLmNsb3NlZCkpIHtcbiAgICAgIHRoaXMuZXZlbnRTdWJzY3JpcHRpb24gPSBmcm9tRXZlbnQodGhpcy53aW5kb3cubmF0aXZlRWxlbWVudCwgJ21lc3NhZ2UnKVxuICAgICAgICAucGlwZShtYXAodGhpcy5vbk1lc3NhZ2UuYmluZCh0aGlzKSkpXG4gICAgICAgIC5zdWJzY3JpYmUoKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgLy8gVW5zdWJzY3JpYmUgaWYgdGhlcmUgYXJlIF9ub18gZXhwb3NlZCBmdW5jdGlvbnM6XG4gICAgaWYgKHRoaXMuZXhwb3NlZEZ1bmNzLnNpemUgPT09IDAgJiYgISF0aGlzLmV2ZW50U3Vic2NyaXB0aW9uICYmICF0aGlzLmV2ZW50U3Vic2NyaXB0aW9uLmNsb3NlZCkge1xuICAgICAgdGhpcy5ldmVudFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgcmVjZWl2ZUNhbGxiYWNrKG1lc3NhZ2U6IENhbGxiYWNrTWVzc2FnZSkge1xuICAgIGNvbnN0IHtjYWxsYmFja0lkLCByZXR1cm5WYWx1ZX0gPSBtZXNzYWdlO1xuICAgIGNvbnN0IHN1YmplY3QgPSB0aGlzLnBlbmRpbmdDYWxsYmFja3MuZ2V0KGNhbGxiYWNrSWQpO1xuICAgIHN1YmplY3QubmV4dChyZXR1cm5WYWx1ZSk7XG4gICAgLy8gVE9ETzogRW5zdXJlIHRoYXQgc3ViamVjdCBpcyBkZXN0cm95ZWQ6XG4gICAgc3ViamVjdC51bnN1YnNjcmliZSgpO1xuICAgIHRoaXMucGVuZGluZ0NhbGxiYWNrcy5kZWxldGUoY2FsbGJhY2tJZCk7XG4gIH1cblxuICBwdWJsaWMgc2VuZENhbGwodGFyZ2V0LCBmdW5jTmFtZSwgLi4ucGFyYW1zOiBhbnlbXSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3QgY2FsbGJhY2tPYnNlcnZhYmxlID0gbmV3IFN1YmplY3QoKTtcbiAgICBjb25zdCBjYWxsYmFja0lkID0gdGhpcy5nZW5lcmF0ZUNhbGxiYWNrSWQoKTtcbiAgICB0aGlzLnBlbmRpbmdDYWxsYmFja3Muc2V0KGNhbGxiYWNrSWQsIGNhbGxiYWNrT2JzZXJ2YWJsZSk7XG5cbiAgICB0YXJnZXQucG9zdE1lc3NhZ2Uoe1xuICAgICAgY2FsbEZ1bmM6IGZ1bmNOYW1lLFxuICAgICAgcGFyYW1zLFxuICAgICAgY2FsbGJhY2tJZFxuICAgIH0gYXMgQ2FsbE1lc3NhZ2UsIHRoaXMudGFyZ2V0T3JpZ2luKTtcblxuICAgIHJldHVybiBjYWxsYmFja09ic2VydmFibGU7XG4gIH1cblxuICBwcml2YXRlIHNlbmRDYWxsYmFjayhmdW5jTmFtZTogRnVuY05hbWUsIGNhbGxiYWNrSWQ6IENhbGxiYWNrSWQsIHJldHVyblZhbHVlLCB0YXJnZXQpIHtcbiAgICB0YXJnZXQucG9zdE1lc3NhZ2Uoe1xuICAgICAgcmV0dXJuRnVuYzogZnVuY05hbWUsXG4gICAgICByZXR1cm5WYWx1ZSxcbiAgICAgIGNhbGxiYWNrSWRcbiAgICB9IGFzIENhbGxiYWNrTWVzc2FnZSwgdGhpcy50YXJnZXRPcmlnaW4pO1xuICB9XG5cbiAgcHJpdmF0ZSBnZW5lcmF0ZUNhbGxiYWNrSWQoKTogQ2FsbGJhY2tJZCB7XG4gICAgcmV0dXJuIE1hdGgucmFuZG9tKCkudG9TdHJpbmcoMzYpLnN1YnN0cmluZygyLCAxNSkgKyBNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDM2KS5zdWJzdHJpbmcoMiwgMTUpO1xuICB9XG5cbiAgcHJpdmF0ZSBvbk1lc3NhZ2UoZXZlbnQ6IE1lc3NhZ2VFdmVudCkge1xuICAgIGlmIChldmVudC5kYXRhLmNhbGxGdW5jKSB7XG4gICAgICB0aGlzLnJlY2VpdmVDYWxsKGV2ZW50LmRhdGEsIGV2ZW50LnNvdXJjZSk7XG4gICAgfSBlbHNlIGlmIChldmVudC5kYXRhLnJldHVybkZ1bmMpIHtcbiAgICAgIHRoaXMucmVjZWl2ZUNhbGxiYWNrKGV2ZW50LmRhdGEpO1xuICAgIH1cbiAgfVxufVxuXG4iXX0=