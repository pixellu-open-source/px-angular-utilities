import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
let PxWindowRef = class PxWindowRef {
    get nativeElement() {
        return window;
    }
};
PxWindowRef.ɵprov = i0.ɵɵdefineInjectable({ factory: function PxWindowRef_Factory() { return new PxWindowRef(); }, token: PxWindowRef, providedIn: "root" });
PxWindowRef = __decorate([
    Injectable({
        providedIn: 'root'
    })
], PxWindowRef);
export { PxWindowRef };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHgtd2luZG93LXJlZi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3B4LWFuZ3VsYXItdXRpbGl0aWVzLyIsInNvdXJjZXMiOlsibGliL2hlbHBlcnMvcHgtd2luZG93LXJlZi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQzs7QUFLekMsSUFBYSxXQUFXLEdBQXhCLE1BQWEsV0FBVztJQUN0QixJQUFJLGFBQWE7UUFDZixPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0NBQ0YsQ0FBQTs7QUFKWSxXQUFXO0lBSHZCLFVBQVUsQ0FBQztRQUNWLFVBQVUsRUFBRSxNQUFNO0tBQ25CLENBQUM7R0FDVyxXQUFXLENBSXZCO1NBSlksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFB4V2luZG93UmVmIHtcbiAgZ2V0IG5hdGl2ZUVsZW1lbnQoKTogV2luZG93IHtcbiAgICByZXR1cm4gd2luZG93O1xuICB9XG59XG4iXX0=