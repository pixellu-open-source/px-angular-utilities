import {Injectable} from '@angular/core';
import {fromEvent, Observable, Subject, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {PxWindowRef} from '../helpers/px-window-ref';

type CallbackId = string;
type FuncName = string;
type AnyFunc = (...args: any[]) => any;

export interface ExposeOptions {
  sourceWindowArg?: boolean;
}

interface ExposedFunc {
  func: AnyFunc,
  options: ExposeOptions
}

interface CallMessage {
  callFunc: FuncName;
  params: any[];
  callbackId: CallbackId;
}

interface CallbackMessage {
  returnFunc: FuncName;
  returnValue: any;
  callbackId: CallbackId;
}

const DEFAULT_EXPOSE_OPTIONS: ExposeOptions = {
  sourceWindowArg: false
};


@Injectable({
  providedIn: 'root'
})
export class PxPostalService {

  targetOrigin = '*';

  private exposedFuncs = new Map<FuncName, ExposedFunc>();
  private pendingCallbacks = new Map<CallbackId, Subject<any>>();
  private eventSubscription: Subscription;

  constructor(private window: PxWindowRef) {
  }

  /**
   * Exposes the function by the given name.
   * @param funcName
   * @param func
   * @param thisArg
   * @param options sourceWindowArg -- if true, the first argument to the exposed function will be the source window.
   */
  expose(funcName: string, func: AnyFunc, thisArg: any, options: ExposeOptions = {}) {
    options = {...DEFAULT_EXPOSE_OPTIONS, ...options};
    this.exposedFuncs.set(funcName, {func: func.bind(thisArg), options});
    this.updateListener();
  }

  unexpose(funcName: string) {
    this.exposedFuncs.delete(funcName);
    this.updateListener();
  }

  unexposeAll() {
    this.exposedFuncs.clear();
    this.updateListener();
  }

  private receiveCall(message: CallMessage, source: any) {
    const {callFunc, params, callbackId} = message;
    const exposedFunc = this.exposedFuncs.get(callFunc);
    console.assert(!!exposedFunc, 'Call function not found');

    let returnValue;

    // If no listeners
    if (!exposedFunc) {
      return;
    }
    // Check if we should provide source window as the first argument to the exposed function:
    if (exposedFunc.options.sourceWindowArg) {
      returnValue = this.exposedFuncs.get(callFunc).func(source, ...params);
    } else {
      returnValue = this.exposedFuncs.get(callFunc).func(...params);
    }

    if (returnValue || returnValue === null) {
      this.sendCallback(callFunc, callbackId, returnValue, source);
    }
  }

  private updateListener() {
    // Remove the subscribe if there are exposed functions:
    if (this.exposedFuncs.size > 0 && (!this.eventSubscription || this.eventSubscription.closed)) {
      this.eventSubscription = fromEvent(this.window.nativeElement, 'message')
        .pipe(map(this.onMessage.bind(this)))
        .subscribe();
      return;
    }
    // Unsubscribe if there are _no_ exposed functions:
    if (this.exposedFuncs.size === 0 && !!this.eventSubscription && !this.eventSubscription.closed) {
      this.eventSubscription.unsubscribe();
    }
  }

  private receiveCallback(message: CallbackMessage) {
    const {callbackId, returnValue} = message;
    const subject = this.pendingCallbacks.get(callbackId);
    subject.next(returnValue);
    // TODO: Ensure that subject is destroyed:
    subject.unsubscribe();
    this.pendingCallbacks.delete(callbackId);
  }

  public sendCall(target, funcName, ...params: any[]): Observable<any> {
    const callbackObservable = new Subject();
    const callbackId = this.generateCallbackId();
    this.pendingCallbacks.set(callbackId, callbackObservable);

    target.postMessage({
      callFunc: funcName,
      params,
      callbackId
    } as CallMessage, this.targetOrigin);

    return callbackObservable;
  }

  private sendCallback(funcName: FuncName, callbackId: CallbackId, returnValue, target) {
    target.postMessage({
      returnFunc: funcName,
      returnValue,
      callbackId
    } as CallbackMessage, this.targetOrigin);
  }

  private generateCallbackId(): CallbackId {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }

  private onMessage(event: MessageEvent) {
    if (event.data.callFunc) {
      this.receiveCall(event.data, event.source);
    } else if (event.data.returnFunc) {
      this.receiveCallback(event.data);
    }
  }
}

