import { TestBed } from '@angular/core/testing';

import {PxWindowRef} from '../helpers/px-window-ref';
import {PxPostalService} from './px-postal.service';
import createSpyObj = jasmine.createSpyObj;

describe('PxPostalService', () => {
  let service: PxPostalService;
  let windowSpy;

  beforeEach(() => {
    windowSpy = createSpyObj('window', ['addEventListener', 'removeEventListener']);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: PxWindowRef,
          useValue: {nativeElement: windowSpy}
        }
      ]
    });
    service = TestBed.inject(PxPostalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should expose and unexpose functions', () => {
    service.expose('func1', () => console.log('func 1 call'), this);
    service.expose('func2', () => console.log('func 2 call'), this);
    service.unexpose('func1');
    expect(windowSpy.addEventListener).toHaveBeenCalled();
    expect(windowSpy.removeEventListener).not.toHaveBeenCalled();
    service.unexpose('func2');
    expect(windowSpy.removeEventListener).toHaveBeenCalled();
  });

  it('should unexpose all functions', () => {
    service.expose('func1', () => console.log('func 1 call'), this);
    service.expose('func2', () => console.log('func 2 call'), this);
    expect(windowSpy.addEventListener).toHaveBeenCalled();
    expect(windowSpy.removeEventListener).not.toHaveBeenCalled();
    service.unexposeAll();
    expect(windowSpy.removeEventListener).toHaveBeenCalled();
  });


});
