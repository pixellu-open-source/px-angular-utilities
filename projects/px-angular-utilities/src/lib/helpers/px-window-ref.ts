import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PxWindowRef {
  get nativeElement(): Window {
    return window;
  }
}
