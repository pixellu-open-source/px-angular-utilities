# Pixellu Angular Utilities

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.10.

## Including the library

Simply run `npm install -s px-angular-utilities@https://bitbucket.org/pixellu-open-source/px-angular-utilities/raw/v0.1.0/dist/px-angular-utilities.tgz`.
Replace the "v0.1.0" with the desired version.

When upgrading to a new version, you will need to clear the entry in the `package-lock.json` of the consuming project before running `npm install` in order to avoid integrity check errors.

## Developing the library in 'watch' mode

1. Run the initial build `npm run build` to create the `dist/px-angular-utilities` directory.
2. Navigate to `dist/px-angular-utilities/`
3. Run `npm link` -- this will create a local global npm package repository linking 'px-angular-utilities' to this directory.
4. Run `npm run build:watch` -- this will watch for changes in the library code and will create a non-production build into the `dist/` directory.
5. Navigate to the Angular project that will consume the library -- `cd $MY_PROJECT_DIR`.
6. Run `npm link px-angular-utilities` -- this will create a symlink under `$MY_PROJECT_DIR/node_modules/px-angular-utilities`
7. Run `ng serve` to build your project `$MY_PROJECT_DIR`. Changes to the library's `dist/` directory will be automatically picked up.

**IMPORTANT:** The consuming project's `angular.json` must contain `"preserveSymlinks": true` under `architect.build.options`,
otherwise the symlinks created by the `npm link px-angular-utilities` will cause `Inject()` errors in the browser console. 

## Publishing a new version

1. Bump the package version number in `projects/px-angular-utilities/package.json` (use the semantic versioning).
2. Run `npm run build:prod` -- this will create a tarball file under the `dist/` directory.
3. Commit to `master` branch and tag with the version (e.g. "v2.0.0").
4. Push :)
5. The production-ready package is served under https://bitbucket.org/pixellu-open-source/px-angular-utilities/raw/v2.0.0/dist/px-angular-utilities.tgz ("v2.0.0" -- commit tag).

